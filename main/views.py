from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import formkegiatan, formpeserta
from .models import Kegiatan, peserta

def halamandepan(request):
    return render(request, 'main/halamandepan.html')

def tambahkegiatan(request):
    form = formkegiatan(request.POST or None)
    if form.is_valid():
        form.save()
        form = Kegiatan()
        return redirect('main:listkegiatan')
    context = {
        'form' : form
    }
    return render(request, 'main/tambahkegiatan.html', context)

def listkegiatan (request):
    kegiatan = Kegiatan.objects.all()
    pesertaid = peserta.objects.all()
    context = {
        'kegiatan' : kegiatan,
        'pesertaid' : pesertaid
    }
    return render(request, 'main/listkegiatan.html', context)

def tambahpeserta(request, my_id):
    form = formpeserta(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            instance = form.save(commit=False)
            instance.kegiatan = Kegiatan.objects.get(id=my_id)
            instance.save()
        return redirect('main:listkegiatan')
        
    response = {
        'form': form
    }
    return render(request, 'main/tambahpeserta.html', response)

def hapuspeserta(request, my_id):
    pesertaid = peserta.objects.get(id=my_id)
    if request.method == "POST":
        pesertaid.delete()
        return redirect('main:listkegiatan')
        
    response = {
        'pesertaid': pesertaid
    }
    return render(request, 'main/hapuspeserta.html', response)



