from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import tambahkegiatan, tambahpeserta, hapuspeserta, listkegiatan, halamandepan
from .models import Kegiatan, peserta
from .forms import formkegiatan, formpeserta


class TestKegiatan(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            namakegiatan = 'test',
            id=1
        )
        self.peserta = peserta.objects.create(
            kegiatan = self.kegiatan,
            namaorang = 'test',
            id=1
        )


    # halaman depan
    def test_url_halamandepan(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    # tambah kegiatan
    def test_url_tambahkegiatan(self):
        response = Client().get('/tambahkegiatan/')
        self.assertEquals(200, response.status_code)
    
    def test_tambahkegiatan_ada_templatenya(self):
	    response = Client().get('/tambahkegiatan/')
	    self.assertTemplateUsed(response, 'main/tambahkegiatan.html')
    
    def test_models_kegiatan_ada_apa_nggak(self):
        new_activity = Kegiatan.objects.create(namakegiatan='mengerjakan story', deskripsi='asik bgt')
        count = Kegiatan.objects.all().count()
        self.assertEqual(count, 2)
        self.assertEqual(str(new_activity),'mengerjakan story')

    def test_formkegiatanya_kalo_kosong(self):
            form = formkegiatan(data={'namakegiatan': '', 'deskripsi': ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['namakegiatan'],
                ["This field is required."]
            )

    def test_views_tambah_kegiatan(self):
       response = self.client.post('/tambahkegiatan/', data= {
           'namakegiatan' : 'ppw',  'deskripsi' : 'asik'
       })
       keg = Client().get(reverse('main:tambahkegiatan'))
       self.assertTemplateUsed(keg, 'base.html')
       self.assertTemplateUsed(keg, 'main/tambahkegiatan.html')
       self.assertEquals(Kegiatan.objects.count(), 2)

    # list kegiatan
    def test_url_listkegiatan(self):
        response = Client().get('/tambahkegiatan/listkegiatan/')
        self.assertEquals(200, response.status_code)

    def test_listkegiatan_ada_templatenya_dan_lengkap(self):
        response = Client().get('/tambahkegiatan/listkegiatan/')
        self.assertTemplateUsed(response, 'main/listkegiatan.html')
        isi = response.content.decode('utf8')
        self.assertIn("Daftar Kegiatan", isi)

    # tambah peserta
    def test_url_tambah_peserta(self):
        response = Client().get(reverse("main:tambahpeserta", args=[self.kegiatan.id]))
        self.assertEquals(response.status_code, 200)
        
    def test_models_pesertanya_ada_apa_nggak(self):
        new_activity = peserta.objects.create(namaorang='hilmy', kegiatan = self.kegiatan)
        count = peserta.objects.all().count()
        self.assertEqual(count, 2)
        self.assertEqual(str(new_activity),'hilmy')
    
    def test_formpesertanya_kalo_kosong(self):
        form = formpeserta(data={'namaorang': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['namaorang'],
            ["This field is required."]
        )
    
    def test_views_tambah_peserta(self):
       pst = Client().get(reverse('main:tambahpeserta',kwargs={'my_id': self.kegiatan.id}))
       self.assertTemplateUsed(pst, 'base.html')
       self.assertTemplateUsed(pst, 'main/tambahpeserta.html')
       response = self.client.post(reverse('main:tambahpeserta',kwargs={'my_id': self.kegiatan.id}),
       data = {
           'namaorang' : 'kapewe'
       })
       self.assertEquals(peserta.objects.count(),2)

    # hapus peserta
    def test_url_hapus_peserta(self):
        response = Client().get(reverse("main:hapuspeserta", args=[self.kegiatan.id]))
        self.assertEquals(response.status_code, 200)

    def test_views_hapus_peserta(self):
        pst = Client().get(reverse('main:hapuspeserta', kwargs={'my_id': self.peserta.id}))
        self.assertTemplateUsed(pst, 'main/hapuspeserta.html')

        response = self.client.post(reverse('main:hapuspeserta', kwargs={'my_id': self.peserta.id}))
        self.assertEquals(peserta.objects.count(),0)




    







    

    


