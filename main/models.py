from django.db import models

class Kegiatan (models.Model):
    namakegiatan = models.CharField(max_length=50)
    deskripsi = models.TextField()

    def __str__(self):
        return self.namakegiatan

class peserta (models.Model):
    namaorang = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.namaorang


