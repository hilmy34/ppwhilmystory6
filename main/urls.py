from django.urls import path

from .views import tambahkegiatan, tambahpeserta, hapuspeserta, listkegiatan, halamandepan

app_name = 'main'

urlpatterns = [
    path('', halamandepan, name='halamandepan'),
    path('tambahkegiatan/', tambahkegiatan, name='tambahkegiatan'),
    path('tambahkegiatan/listkegiatan/', listkegiatan, name='listkegiatan'),
    path('listkegiatan/<int:my_id>/tambahpeserta/', tambahpeserta, name='tambahpeserta'),
    path('listkegiatan/<int:my_id>/hapuspeserta/', hapuspeserta, name='hapuspeserta'),
]
