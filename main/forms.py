from django import forms
from .models import Kegiatan, peserta

class formkegiatan(forms.ModelForm):

    class Meta:
        model = Kegiatan
        fields = ['namakegiatan', 'deskripsi']

    title_attrs = {
        'type': 'text',
        'class': 'form-namakegiatan',
        'placeholder':'Masukan nama kegiatan...'
    }

    desc_attrs = {
        'type': 'text',
        'class': 'form-deskripsi',
        'placeholder':'Masukan deskripsi...'
    }

    namakegiatan = forms.CharField(required=True, widget=forms.TextInput(attrs=title_attrs))
    deskripsi = forms.CharField(widget=forms.Textarea(attrs=desc_attrs))

    namakegiatan.widget.attrs.update({'class':'form-control'})
    deskripsi.widget.attrs.update({'class':'form-control'})

class formpeserta(forms.ModelForm):

    class Meta:
        model = peserta
        fields = ['namaorang']

    title_attrs = {
    'type': 'text',
    'class': 'form-namapeserta',
    'placeholder':'Masukan nama peserta...'
}

    namaorang = forms.CharField(required=True,widget=forms.TextInput(attrs=title_attrs))
    namaorang.widget.attrs.update({'class':'form-control'})


